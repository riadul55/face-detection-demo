import 'package:demo_test/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HomeView'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ElevatedButton(onPressed: () {
              controller.chooseImageFromGallery();
            }, child: const Text("Get Image")),
            Obx(() => Text(controller.selectedImage.value)),
            const SizedBox(height: 20),
            ElevatedButton(onPressed: () {
              if (controller.selectedImage.isNotEmpty) {
                Get.toNamed(Routes.CAMERA, arguments: controller.selectedImage.value);
              } else {
                Get.rawSnackbar(title: "Warning!", message: "No image selected!", backgroundColor: Colors.orange);
              }
            }, child: const Text("Go Camera")),
          ],
        ),
      ),
    );
  }
}

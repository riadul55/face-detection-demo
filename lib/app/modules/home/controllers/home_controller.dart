import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class HomeController extends GetxController {

  var selectedImage = "".obs;

  @override
  void onInit() async {
    super.onInit();
    Map<Permission, PermissionStatus> statuses = await [
      Permission.camera,
      Permission.storage,
      Permission.accessMediaLocation,
      Permission.photos,
    ].request();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  /// gallery
  Future<void> chooseImageFromGallery() async {
    bool isStoragePermitted = await isStoragePermissionGranted();
    if (isStoragePermitted) {
      final ImagePicker picker = ImagePicker();
      final XFile? image = await picker.pickImage(source: ImageSource.gallery);
      if (image != null) {
        selectedImage(image.path);
      }
    }
  }

  Future<bool> isStoragePermissionGranted() async {
    final status = await Permission.storage.request();
    if (status.isGranted) {
      return true;
    }
    return false;
  }

}

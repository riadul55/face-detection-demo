import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tflite_flutter/tflite_flutter.dart';

import 'ml_service.dart';

class CamerasController extends GetxController {

  final String imagePath = Get.arguments;

  late CameraController controller;
  late List<CameraDescription> cameras;

  late FaceDetector _faceDetector;
  final MLService _mlService = MLService();

  List<Face> facesDetected = [];

  var isCameraInitialized = false.obs;

  @override
  void onInit() {
    super.onInit();
    initCamera();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    controller.dispose();
  }


  Future<void> initCamera() async {
    bool isCameraPermitted = await isCameraPermissionGranted();
    if (isCameraPermitted) {
      cameras = await availableCameras();
      controller = CameraController(cameras[1], ResolutionPreset.high);
      await controller.initialize();
      isCameraInitialized(true);
      _faceDetector = GoogleMlKit.vision.faceDetector(
        FaceDetectorOptions(
          enableClassification: true,
          enableTracking: true,
          enableLandmarks: true,
          minFaceSize: 0.15,
          performanceMode: FaceDetectorMode.accurate,
        ),
      );

      controller.startImageStream((CameraImage image) {
        _predictFacesFromImage(image: image);
      });
    } else {
      Get.rawSnackbar(title: "Error",
          message: "Camera permission not granted!",
          backgroundColor: Colors.red);
    }
  }


  Future<bool> isCameraPermissionGranted() async {
    final status = await Permission.camera.request();
    if (status.isGranted) {
      return true;
    }
    return false;
  }
  //
  // Future<void> detectFacesFromImage(CameraImage image) async {
  //   InputImageData _firebaseImageMetaData = InputImageData(
  //       size: Size(image.width.toDouble(), image.height.toDouble()),
  //       imageRotation: rotationIntToImageRotation(
  //           controller.description.sensorOrientation),
  //       inputImageFormat: InputImageFormat.bgra8888,
  //       planeData: image.planes.map((plane) =>
  //           InputImagePlaneMetadata(bytesPerRow: plane.bytesPerRow)).toList());
  //
  //   InputImage _firebaseVisionImage = InputImage.fromBytes(
  //       bytes: image.planes[0].bytes, inputImageData: _firebaseImageMetaData);
  //
  //   var result = await _faceDetector.processImage(_firebaseVisionImage);
  //   if (result.isNotEmpty) {
  //     _faces = result;
  //   }
  // }
  //
  //
  // Future<List<Face>> getFaceFromPath(String path) async {
  //   final inputImage = InputImage.fromFilePath(path);
  //   var result = await _faceDetector.processImage(inputImage);
  //   return result;
  // }

  //   InputImageRotation rotationIntToImageRotation(int rotation) {
  //   switch (rotation) {
  //     case 90:
  //       return InputImageRotation.rotation90deg;
  //     case 180:
  //       return InputImageRotation.rotation180deg;
  //     case 270:
  //       return InputImageRotation.rotation270deg;
  //     default:
  //       return InputImageRotation.rotation0deg;
  //   }
  // }
  //
  //
  // void _predictFacesFromImage(CameraImage image) async {
  //   // var selectedFace = await getFaceFromPath();
  //   await _mlService.predict(imagePath, await _takePicture());
  //   // if (result) {
  //   //   Get.rawSnackbar(title: "Success", message: "Faces matched!", backgroundColor: Colors.green);
  //   // } else {
  //   //   Get.rawSnackbar(title: "failed", message: "Faces not matched!", backgroundColor: Colors.red);
  //   // }
  // }
  //
  // Future<String> _takePicture() async {
  //   try {
  //     final image = await controller.takePicture();
  //     final directory = await getApplicationDocumentsDirectory();
  //     final path = '${directory.path}/image.jpg';
  //
  //     final file = File(path);
  //     await file.writeAsBytes(image.readAsBytes() as List<int>);
  //
  //     return path;
  //   } catch (e) {
  //     print(e);
  //
  //     return '';
  //   }
  // }

  InputImageRotation rotationIntToImageRotation(int rotation) {
    switch (rotation) {
      case 90:
        return InputImageRotation.rotation90deg;
      case 180:
        return InputImageRotation.rotation180deg;
      case 270:
        return InputImageRotation.rotation270deg;
      default:
        return InputImageRotation.rotation0deg;
    }
  }

  Future<void> detectFacesFromImage(CameraImage image) async {
    InputImageData _firebaseImageMetadata = InputImageData(
      imageRotation: rotationIntToImageRotation(
          controller.description.sensorOrientation),
      inputImageFormat: InputImageFormat.bgra8888,
      size: Size(image.width.toDouble(), image.height.toDouble()),
      planeData: image.planes.map(
            (Plane plane) {
          return InputImagePlaneMetadata(
            bytesPerRow: plane.bytesPerRow,
            height: plane.height,
            width: plane.width,
          );
        },
      ).toList(),
    );

    InputImage _firebaseVisionImage = InputImage.fromBytes(
      bytes: image.planes.first.bytes,
      inputImageData: _firebaseImageMetadata,
    );
    var result = await _faceDetector.processImage(_firebaseVisionImage);
    if (result.isNotEmpty) {
      facesDetected = result;
    }
  }

  Future<void> _predictFacesFromImage({required CameraImage image}) async {
    await detectFacesFromImage(image);
    if (facesDetected.isNotEmpty) {
      bool isMatched = await _mlService.predict(
          image,
          facesDetected.first,
        imagePath
      );
      if (isMatched) {
        Get.defaultDialog(title: "Face matched", onConfirm: () {
          Get.back(closeOverlays: true);
        });
      } else {
        Get.defaultDialog(title: "Face not matched", onConfirm: () {
          Get.back(closeOverlays: true);
        });
      }
    } else {
      Get.rawSnackbar(title: "face not detected!", backgroundColor: Colors.red);
    }
    await takePicture();
  }

  Future<void> takePicture() async {
    if (facesDetected.isNotEmpty) {
      await controller.stopImageStream();
      XFile file = await controller.takePicture();
      file = XFile(file.path);
      controller.setFlashMode(FlashMode.off);
    }
  }


}
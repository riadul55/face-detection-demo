import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/camera_controller.dart';

class CameraView extends GetView<CamerasController> {
  const CameraView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CameraView'),
        centerTitle: true,
      ),
      body: Obx(() => controller.isCameraInitialized.value
          ? CameraPreview(controller.controller)
          : const Center(
              child: Text("Camera not initiazed!"),
            )),
    );
  }
}
